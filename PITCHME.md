---?image=img/blue-abstract.svg&size=cover


# Programowanie wielowątkowe w C++

Krystian Piękoś

---

## Informacje organizacyjne

- Czas trwania szkolenia: 
    - 9:00 - 16:00

- Materiały szkoleniowe:
    - https://infotraining.bitbucket.io/cpp-thd
    - [repozytorium GIT](https://bitbucket.org/infotraining)

- Przerwy
- Lista obecności
- Ankieta

---

## Co to jest współbieżność?

* Dwa działania lub więcej wykonywane jednocześnie | 
* Jeden procesor |
    - szybkie przełączanie zadań - multitasking
* Wiele procesorów |
    - sprzętowa równoległość 

---

## Rodzaje współbieżności

* Multiprocessing |
* Multithreading |
* Vector parallelism (SIMD) |

---

## Proces

* Egzemplarz wykonywanego programu
* Kontener wątków w chronionej przestrzeni adresowej
* System operacyjny przydziela procesowi odpowiednie zasoby
* Za zarządzanie procesami odpowiada jądro systemu operacyjnego
  - system operacyjny zarządza priorytetami procesów

+++

### Komunikacja między procesami

* Sygnały
* Gniazda (sockets)
* Pliki
* Potoki
* Pamięć współdzielona
* Kolejki - *message queues*

---

## Wątek

niezależny ciąg instrukcji wykonywany współbieżnie w ramach jednego procesu

+++

* Wszystkie wątki działające w danym procesie współdzielą przestrzeń adresową oraz zasoby systemowe
  - pamięć (heap, static storage)
  - pliki wymagane przez aplikację
  - cykle CPU (multitasking)
  - gniazda (sockets)

+++

### Natywne API dla wątków

* MS Windows - Win32 API
* Linux, BSD - pthread

+++

### Cross-platform C++ API

* Boost.Thread
* Threading Building Blocks - Intel
* Standard C++ Library - od C++11

+++

### g++ - opcje kompilacji

@snap[code-noblend fragment]
```bash
g++ -std=c++11 -pthread main.cpp
```
@snapend

---

## Po co używać współbieżności

* Wydajność |
  - Chcemy wykorzystać całą moc maszyny wieloprocesorowej |
  - Chcemy podnieść skalowalność aplikacji |
  - „The free lunch is over” | 
* Lepsza responsywność aplikacji |
  - Unikanie blokujących operacji I/O | 
  - Blokowanie interfejsu użytkownika |

--- 

## Kiedy nie używać współbieżności

Należy unikać wielowątkowości, gdy korzyści nie są warte kosztów

+++

## Kiedy nie używać współbieżności

* Kod wielowątkowy jest dużo bardziej skomplikowany - trudniejszy do pisania, czytania i testowania niż kod jednowątkowy |
* Większa złożoność, więcej błędów, które są trudne do wykrycia (często trudne do odtworzenia) |
* Narzuty związane z zarządzaniem wątkami mogą drastycznie obniżyć wydajność |
  - thread oversubsription + context switching |
  - cache ping-pong |
  - false sharing |

---

## Testowanie aplikacji wielowątkowych

* Kod jednowątkowy może być testowany jednostkowo (unit tests) |
  - powtarzalne wyniki dla testów wykonywanych w izolacji
* Kod wielowątkowy jest trudno testowalny jednostkowo |
  - błędy (np. race conditions) nie są deterministyczne i są ciężko reprodukowalne
  - problemy pojawiają często przy dużym obciążeniu lub zwielokrotnieniu wątków

+++ 

## Testowanie aplikacji wielowątkowych

* Dobrze jest zapewnić możliwość skalowania ilości wątków do jednego (single threaded code) aby wykluczyć inne przyczyny błędów

---

# Wątki - std::thread

---

## Zarządzanie wątkami

@ul[fragment]
* Klasa ``std::thread`` jest odpowiedzialna za tworzenie obiektów, które uruchamiają wątki systemowe i zarządzają nimi
* Każda instancja klasy ``std::thread`` reprezentuje
  - pojedynczy wątek, utworzony przez system operacyjny
  - lub tzw. **wątek pusty** (*not-a-thread*)
@ulend 

+++

### Wątek pusty

@ul[fragment]
* Instancja ``std::thread`` utworzona konstruktorem domyślnym 
* lub obiekt wątku po wykonaniu na nim operacji ``std::move()``
@ulend

+++

## std::thread + std::move

@ul[fragment]
* Obiekty wątków nie są kopiowalne - *non-copyable*
* Wątki systemowe mogą być przenoszone między obiektami 
  - semantyka przenoszenia - *move semantics*
  - w celu transferu wątku, który jest l-value należy użyć funkcji ``std::move``
@ulend

---

## Tworzenie wątku (1)

```c++
#include <thread>

void my_thread_func()
{
    std::cout << "My first thread..." << std::endl;
}

int main()
{
    std::thread t(&my_thread_func);
    t.join();
}
```
@[1]
@[3-6]
@[10-11]

+++

## Tworzenie wątku (2)

```c++
class BackgroundTask
{
public:
    void operator()() const
    {
        std::cout << "Hello from a thread..." << std::endl;
    }
};

int main()
{
    BackgroundTask bt;
    std::thread t1(bt);
    std::thread t2(BackgroundTask());
    t1.join();
    t2.join();
}
```
@[1-9]
@[12-16]


+++

## Tworzenie wątku (3)

```c++
std::thread t([] { 
    std::cout << "My first thread..." << std::endl; 
});

t.join();

```

---

## Dołączenie do wątku

@ul
* Aby poczekać na zakończenie wykonywania zadania przez dany wątek, należy wywołać na jego rzecz metodę ``join()``
*  ``join()`` blokuje (wstrzymuje wywołanie) bieżący wątek, aż do czasu ukończenia zadania wykonywanego przez wskazany wątek 
@ulend

+++

```c++
int main()
{
    BackgroundTask bt;

    std::thread t(bt);
    t.join(); // wstrzymanie wykonania wątku głównego (main),
              // aż do czasu zakończenia zadania w wątku t

    assert(!thd.joinable());
}
```
@[6-7]

+++

### C++ Core Guidelines - join

[Prefer gsl::joining_thread over std::thread](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rconc-joining_thread)

---

## Odłączanie wątków od obiektów

* Wątki odłączone od obiektu:
  - wątki tła
  - demony
* Aby utworzyć wątek tła należy na rzecz obiektu reprezentującego dany wątek wywołać metodę ``detach()``

+++

@snap[midpoint]
```c++
std::thread thd(&do_background_work);

thd.detach();

assert(!thd.joinable());
```
@snapend
@[3]
@[5] 

+++

### C++ Core Guidelines - detach

[Don’t detach() a thread](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rconc-detached_thread)

---

## Destruktor wątku

@ul
* Jeśli obiekt wątku jest skojarzony z wątkiem systemowym wywołanie metody ``joinable()`` zwraca ``true``.
* Destrukcja obiektu wątku jest bezpieczna, jeśli obiekt wątku nie jest skojarzony z wątkiem systemowym. 
* W przeciwnym wypadku wywołana jest funkcja ``std::terminate()``
@ulend
 
+++

## Destruktor wątku

@ul
* Obiekt wątku nie jest skojarzony z wątkiem systemowym:
  - jeśli został utworzony za pomocą konstruktora domyślnego
  - został przeniesiony do innego obiektu - np. jest po wywołaniu ``std::move()``
  - została wcześniej wywołana operacja ``join()``
  - została wcześniej wywołana operacja ``detach()``
@ulend 

+++

## Destrukcja wątku

Standard C++ wymusza jawne wywołanie ``join()`` lub ``detach()`` przed wywołaniem destruktora wątku, który jest skojarzony z wątkiem systemowym. 

---

## Przekazywanie parametrów do wątków

Na 3 sposoby

+++

### Sposób 1

Przekazując argumenty jako kolejne (po funkcji lub obiekcie funkcyjnym) parametry konstruktora ``std::thread``

Podane argumenty są forwardowane (kopiowane lub przenoszone) do uruchamianego wątku.

+++
@snap[midpoint span-90]
```c++
void process(std::vector<int>& data, int index)
{
    //... processing data

    data[index] = calculated_value;
}

//...

std::vector<int> vec(2);
const int slot_index = 1;

std::thread thd{&process, std::ref(vec), slot_index};

thd.join();
```
@[1]
@[10-13]
@snapend

+++

### Przekazanie referencji

* Jeśli wymagane jest przekazanie referencji do funkcji uruchamianej w nowym wątku, należy użyć standardowych wraperów dla:
  * referencji - ``std::ref()`` 
  * referencji do stałej - ``std::cref()``

+++

### Sposób 2

Wykorzystując obiekt domknięcia, który przechwytuje zmienne będące argumentami wywoływanej funkcji

+++
@snap[midpoint span-90]
```c++
void process(std::vector<int>& data, int index)
{
    //... processing data

    data[index] = calculated_value;
}

//...

std::vector<int> vec(2);
const int slot_index = 1;

std::thread thd{[&vec, slot_index] { process(vec, slot_index); }};

thd.join();
```
@[1]
@[10-13]
@snapend

+++ 

### Sposób 3

Konstrukcja obiektu funkcyjnego przekazywanego do konstruktora instancji ``std::thread``:

+++

@snap[midpoint span-90]
```c++
class Processor
{
    std::vector<int>& data_;
    const int slot_index_;
public: 
    Processor(std::vector<int> data, int index) 
        : data_{data}, slot_index_{index}
    {}

    void operator()() 
    {
        process(data_, slot_index_);
    }
};

std::vector<int> vec(2);
const int slot_index = 1;

Processor processor_1{vec, slot_index};
std::thread thd{processor_1};

thd.join();
```
@[6-8]
@[19-20]
@snapend

---

## Problem wiszących referencji

Przy przekazywaniu parametrów do wątków należy unikać wiszących referencji (dangling references)

[C++ Core Guidelines [CP.31]](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rconc-data-by-value)

---

## std::thread & move semantics

Klasa ``std::thread`` implementuje tylko semantykę przenoszenia

+++

@snap[midpoint span-80]
```c++
void task() { /* implementation */ }

std::thread create_thread()
{
    std::thread thd(&task);
    return thd;
}

std::vector<std::thread> threads;

// implicit move of r-value
threads.push_back(create_thread());

// explicit move of l-value
std::thread thd(&task);
threads.push_back(std::move(thd));

for(auto& thd : threads)
    thd.join();

```
@[3-7]
@[12](implicit move of std::thread instance)
@[15-16](explicit move of std::thread instance)
@snapend

---

## Obsługa wyjątków w wątkach

@box[fragment]( Jeżeli z funkcji uruchomionej w osobnym wątku wydostanie się wyjątek, zostanie wywołana funkcja ``std::terminate()``)

@box[fragment](W celu prawidłowej obsługi wyjątków, należy przechwycić rzucony wyjątek i jeżeli istnieje taka potrzeba, przekazać go do wątku rodzica wykorzystując klasę ``std::exception_ptr``) 

+++

### Klasa std::exception_ptr

@ul[text-08]
* Umożliwia przechowanie wskaźnika do obiektu wyjątku, który został zgłoszony instrukcją ``throw`` i przechwycony funkcją ``std::current_exception()``
* Instancja ``std::exception_ptr`` może być przekazana do innej funkcji, również takiej, która uruchomiona jest w osobnym wątku
* Obsługa przekazanego przez wskaźnik wyjątku jest możliwa przy pomocy funkcji ``std::rethrow_exception()`` , która powoduje ponowne rzucenie wyjątku
@ulend

+++

### Klasa std::exception_ptr

@ul[text-08]
* Domyślnie skonstruowany obiekt ``std::exception_ptr`` ma wartość ``nullptr``
* Dwa obiekty są uznawane za równe, jeśli są puste lub wskazują na ten sam obiekt wyjątku.
* Instancja ``std::exception_ptr`` jest konwertowalna do wartości logicznej
@ulend

+++

```c++
void background_work(std::exception_ptr& excpt)
{
    try
    {
        may_throw();

        // ...
    }
    catch(...)
    {
        excpt = std::current_exception();
    }
}
```
@[3-12]
@[5](some function throws an exception)
@[9-12](exception is caught & stored as an instance of the exception_ptr)

+++

```c++
int main()
{
    std::exception_ptr thd_exception;

    std::thread thd(&background_work, std::ref(thd_exception));
    thd.join();

    try
    {
        std::rethrow_exception(e);
    }
    catch(const std::runtime_error& e)
    {
        // ... handling an error
    }
}
```
@[3](object that can store an exception thrown in another thread)
@[5](is passed by reference to spawned thread)
@[6](waiting for the end of threads execution)
@[8-16](handling an exception in the main thread)

---

## Funkcje i klasy pomocnicze

+++

### std::thread::hardware_concurrency()

Statyczna metoda zwracająca ilość dostępnych wątków sprzętowych. 

Zwykle podawana jest ilość procesorów, rdzeni, itp. Jeżeli informacja nie jest dostępna zwraca wartość ``0``.


@snap[code-noblend fragment ]
```c++
auto hardware_threads_count = 
    std::max(1u, std::thread::hardware_concurrency());

std::vector<std::thread> threads(hardware_threads_count);
```
@snapend

+++ 

### Przestrzeń nazw ``std::this_thread``

@ul[text-08]
* ``sleep_for(const chrono::duration<R, P>& sleep_duration)``
  - wstrzymuje wykonanie bieżącego wątku na (przynajmniej) określony interwał czasu
* ``sleep_until(const chrono::time_point<C, D>& sleep_time)``
  - blokuje wykonanie wątku przynajmniej do podanego jako parametr punktu czasu
* ``yield()``
  - funkcja umożliwiające podjęcie próby wywłaszczenia bieżącego wątku i przydzielenia czasu procesora innemu wątkowi
* ``get_id()`` 
  - zwraca obiekt typu ``std::thread::id`` reprezentujący identyfikator bieżącego wątku
@ulend

+++

### Identyfikator wątku

``std::thread::id`` jest lekką, trywialnie kopiowalną klasą, która opakowuje unikalny identyfikator wątku.

@box[fragment](Celem klasy jest możliwość użycia jej jako klucza w kontenerach asocjacyjnych)

+++

### Klasa ``std::thread::id``

@ul[text-08 span-90]
* Domyślny konstruktor tworzy obiekt identyfikujący tzw. pusty wątek (Not-A-Thread)
* Instancje ``thread::id`` są kopiowalne, porównywalne oraz hashowalne
  - w klasie zdefiniowany jest pełen zestaw operatorów porównania
  - biblioteka standardowa definiuje pomocniczą klasę ``std::hash<std::thread::id>``, która umożliwia przechowanie wątku w kontenerach hashujących (np. ``unordered_map``)
* Klasa posiada operator wyjścia do strumienia ``operator<<``
@ulend